package tdd.training.mra;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRover {
	
	private int planetX;
	private int planetY;
	private List<String> planetObstacles;
	private int positionX;
	private int positionY;
	private String directionRover;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		int positionX = 0;
		int positionY = 0;
		directionRover = "N";
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		boolean returnValue = false;
		
		for(int i = 0; i < planetObstacles.size() &&  !returnValue; i++) {
			if(coordinatesToString(x,y).equals(planetObstacles.get(i))) {
				returnValue = true;
			}
		}
		return returnValue;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		String noteOfTheEncounteredObstacle = "";
		String auxString = "";
		
		for(int i = 0; i < commandString.length(); i++) {
			if(commandString.charAt(i) == ('r')) {
				changeDirectionMovingRight();
			}else if(commandString.charAt(i) == ('l')) {
				changeDirectionMovingLeft();
			}else if(commandString.charAt(i) == ('f')) {
				auxString = changePositionsMovingForwardAndTakeNotes();
				if(!noteOfTheEncounteredObstacle.equals(auxString)) {
					noteOfTheEncounteredObstacle += auxString;
				}
			}else if(commandString.charAt(i) == ('b')) {
				auxString = changePositionsMovingBackwardAndTakeNotes();
				if(!noteOfTheEncounteredObstacle.equals(auxString)) {
					noteOfTheEncounteredObstacle += auxString;
				}
			}
		}
		
		return statusToString() + noteOfTheEncounteredObstacle;
	}
	
	private String coordinatesToString(int x, int y) {
		return "(" + Integer.toString(x) + "," + Integer.toString(y) + ")";
	}
	
	private String statusToString() {
		return "(" + Integer.toString(positionX) + "," + Integer.toString(positionY) + "," + directionRover + ")";
	}
	
	private String changePositionsMovingForwardAndTakeNotes() throws MarsRoverException {
		String noteOfTheEncounteredObstacle = "";
		
		if(directionRover.equals("N")) {
			if(!planetContainsObstacleAt(positionX, positionY + 1)){
				positionY++;
			}else {
				noteOfTheEncounteredObstacle = coordinatesToString(positionX, positionY + 1);
			}
		}else if(directionRover.equals("E")) {
			if(!planetContainsObstacleAt(positionX + 1, positionY)){
				positionX++;
			}else {
				noteOfTheEncounteredObstacle = coordinatesToString(positionX + 1, positionY);
			}
		}else if(directionRover.equals("S")) {
			if(!planetContainsObstacleAt(positionX, positionY - 1)){
				positionY--;
			}else {
				noteOfTheEncounteredObstacle = coordinatesToString(positionX, positionY - 1);
			}
		}else if(directionRover.equals("W")) {
			if(!planetContainsObstacleAt(positionX - 1, positionY)){
				positionX--;
			}else {
				noteOfTheEncounteredObstacle = coordinatesToString(positionX - 1, positionY);
			}
		}
		
		roverGoBeyondTheEdges();
		return noteOfTheEncounteredObstacle;
	}
	
	private String changePositionsMovingBackwardAndTakeNotes() throws MarsRoverException {
		String noteOfTheEncounteredObstacle = "";
		
		if(directionRover.equals("N")) {
			if(!planetContainsObstacleAt(positionX, positionY - 1)){
				positionY--;
			}else {
				noteOfTheEncounteredObstacle = coordinatesToString(positionX, positionY - 1);
			}
		}else if(directionRover.equals("E")) {
			if(!planetContainsObstacleAt(positionX - 1, positionY)){
				positionX--;
			}else {
				noteOfTheEncounteredObstacle = coordinatesToString(positionX - 1, positionY);
			}
		}else if(directionRover.equals("S")) {
			if(!planetContainsObstacleAt(positionX, positionY + 1)){
				positionY++;
			}else {
				noteOfTheEncounteredObstacle = coordinatesToString(positionX, positionY + 1);
			}
		}else if(directionRover.equals("W")) {
			if(!planetContainsObstacleAt(positionX + 1, positionY)){
				positionX++;
			}else {
				noteOfTheEncounteredObstacle = coordinatesToString(positionX + 1, positionY);
			}
		}
		
		roverGoBeyondTheEdges();
		return noteOfTheEncounteredObstacle;
	}
	
	private void changeDirectionMovingRight() {
		if(directionRover.equals("N")) {
			directionRover = "E";
		}else if(directionRover.equals("E")) {
			directionRover = "S";
		}else if(directionRover.equals("S")) {
			directionRover = "W";
		}else if(directionRover.equals("W")) {
			directionRover = "N";
		}
	}
	
	private void changeDirectionMovingLeft() {
		if(directionRover.equals("N")) {
			directionRover = "W";
		}else if(directionRover.equals("E")) {
			directionRover = "N";
		}else if(directionRover.equals("S")) {
			directionRover = "E";
		}else if(directionRover.equals("W")) {
			directionRover = "S";
		}
	}
	
	private void roverGoBeyondTheEdges() {
		if(positionY < 0) {
			positionY = planetY + positionY;
		}else if (positionY > 9) {
			positionY = positionY - planetY ;
		}
			
		if(positionX < 0) {
			positionX = planetX + positionX;
		}else if (positionX > 9) {
			positionX = positionX - planetX;
		}
	}

	public void setPositions(int positionX, int positionY) {
		this.positionX = positionX;
		this.positionY = positionY;
	}

	public void setDirection(String directionRover) {
		this.directionRover = directionRover;
	}

}
