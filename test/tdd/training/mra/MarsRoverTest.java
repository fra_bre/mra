package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testInputObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		assertTrue(rover.planetContainsObstacleAt(0, 1));
	}
	
	@Test
	public void testInputObstaclesForBiggerPlanet() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertTrue(rover.planetContainsObstacleAt(4, 7));
	}
	
	@Test
	public void testExecuteCommandForNullString() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,N)", rover.executeCommand(""));
	}
	
	@Test
	public void testExecuteCommandForNullStringAndSetDirection() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,N)", rover.executeCommand(""));
	}
	
	@Test
	public void testExecuteCommandToTurnRightTheRover() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,E)", rover.executeCommand("r"));
	}
	
	@Test
	public void testExecuteCommandToTurnLeftTheRover() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,W)", rover.executeCommand("l"));
	}
	
	@Test
	public void testExecuteCommandMoveForwardTheRover() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositions(7,6);
		assertEquals("(7,7,N)", rover.executeCommand("f"));
	}
	
	@Test
	public void testExecuteCommandMoveBackwardTheRover() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositions(5,8);
		rover.setDirection("E");
		assertEquals("(4,8,E)", rover.executeCommand("b"));
	}
	
	@Test
	public void testExecuteCommandMoveForwardTheRoverForAllDirection() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositions(5,8);
		rover.setDirection("E");
		assertEquals("(6,8,E)", rover.executeCommand("f"));
	}
	
	@Test
	public void testExecuteCommandMoveBackwardTheRoverForAllDirection() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositions(7,8);
		rover.setDirection("W");
		assertEquals("(8,8,W)", rover.executeCommand("b"));
	}
	
	@Test
	public void testExecuteCombinationOfSingleCommands() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}
	
	@Test
	public void secondTestExecuteCombinationOfSingleCommands() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositions(7,8);
		rover.setDirection("W");
		assertEquals("(5,6,S)", rover.executeCommand("fflff"));
	}
	
	@Test
	public void thirdTestExecuteCombinationOfSingleCommands() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositions(5,6);
		rover.setDirection("E");
		assertEquals("(9,7,W)", rover.executeCommand("flflbbb"));
	}
	
	@Test
	public void testRoverGoBeyondTheEdgesMovingBackward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,9,N)", rover.executeCommand("b"));
	}
	
	@Test
	public void testRoverGoBeyondTheEdgesMovingForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositions(0,9);
		rover.setDirection("W");
		assertEquals("(9,9,W)", rover.executeCommand("f"));
	}
	
	@Test
	public void testRoverGoBeyondTheEdgesMovingForwardForMoreTimes() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPositions(0,9);
		rover.setDirection("W");
		assertEquals("(7,9,W)", rover.executeCommand("fff"));
	}
	
	@Test
	public void testRoverEncounterAnObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(1,2,E)(2,2)", rover.executeCommand("ffrfff"));
	}
	
	@Test
	public void testRoverEncounterAnObstacleIn4and4() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,4)");
		planetObstacles.add("(2,9)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(2,3,E)(0,4)", rover.executeCommand("fffffrff"));
	}
	
	@Test
	public void testRoverEncounterAnMultipleObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(1,1,E)(2,2)(2,1)", rover.executeCommand("ffrfffrflf"));
	}
}
